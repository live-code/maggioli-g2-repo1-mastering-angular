import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export interface COnfig {
  type: string;
  color: string;
}

@Component({
  selector: 'ma-card',
  template: `
    <div class="card" [ngClass]="'mb-' + marginBottom">
      <div 
        class="card-header" 
        [ngClass]="headerCls"
        (click)="toggle.emit()"
      >
        {{title}} - {{isOpen}}
        
        <i 
          *ngIf="icon" 
          class="pull-right" 
          [ngClass]="icon"
          (click)="iconClick.emit()"
        ></i>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent  {
  @Input() config: COnfig ;
  @Input() title = 'Hello';
  @Input() headerCls: string;
  @Input() icon: string;
  @Input() isOpen: boolean;
  @Input() marginBottom: 0 | 1 | 2 | 3 | 4 | 5 = 3;
  @Output() iconClick: EventEmitter<void> = new EventEmitter();
  @Output() toggle: EventEmitter<void> = new EventEmitter();
}
