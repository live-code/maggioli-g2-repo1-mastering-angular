import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ma-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item" 
        *ngFor="let item of items" 
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link" 
          [ngClass]="{'active': item.id === active?.id}"
        >{{item[labelField]}}</a>
      </li>
    </ul>
  `,
})
export class TabbarComponent<T> {
  @Input() labelField = 'label';
  @Input() items: T[];
  @Input() active: T;
  @Output() tabClick: EventEmitter<T> = new EventEmitter();
}
