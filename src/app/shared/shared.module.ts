import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';

@NgModule({
  declarations: [
    CardComponent, TabbarComponent
  ],
  exports: [
    CardComponent, TabbarComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
