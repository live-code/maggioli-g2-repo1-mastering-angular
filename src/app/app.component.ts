import { Component, ViewEncapsulation } from '@angular/core';
import { UsersService } from './features/users/services/users.service';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'ma-root',
  template: `
    <ma-navbar></ma-navbar>
    <hr>
    <div class="container mt-3">
     <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
  constructor(private router: Router) {
    router.events
      .pipe(
        filter(res => res instanceof NavigationEnd),
        map((res: NavigationEnd) => res.url)
      )
      .subscribe((url: string) => {
        console.log(url)
      })
  }
}
