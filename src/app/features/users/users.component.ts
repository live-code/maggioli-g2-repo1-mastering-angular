import { Component, OnInit } from '@angular/core';
import { UsersService } from './services/users.service';

@Component({
  selector: 'ma-users',
  template: `
    <div class="row">
      <div class="col">
        <button (click)="titleValue = { text: 'Pippo'}">change title</button>
        <ma-users-form
          [title]="titleValue"
          [active]="userService.active"
          (save)="userService.save($event)"
          (clean)="userService.resetHandler()"
        ></ma-users-form>
      </div>
      <div class="col">
        <ma-users-list
          [users]="userService.users"
          [active]="userService.active"
          (setActive)="userService.setActive($event)"
          (delete)="userService.deleteUser($event)"
          (goto)="userService.goToSheet($event)"
        >
        </ma-users-list>
      </div>
    </div>
    
  `,
})
export class UsersComponent implements OnInit {
  titleValue = { text: 'Form User' }
  constructor(public userService: UsersService) { }

  ngOnInit(): void {
    this.userService.getAll();
  }

}
