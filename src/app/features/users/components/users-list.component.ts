import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ma-users-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ng-content></ng-content>
    <ma-users-list-item
      *ngFor="let user of users"
      [user]="user"
      [selected]="user.id === active.id"
      (setActive)="setActive.emit($event)"
      (delete)="delete.emit($event)"
      (goto)="goto.emit($event)"
    >
    </ma-users-list-item>
  `,
})
export class UsersListComponent {
  @Input() users: User[];
  @Input() active: User;
  @Output() setActive: EventEmitter<User> = new EventEmitter<User>()
  @Output() delete: EventEmitter<number> = new EventEmitter<number>()
  @Output() goto: EventEmitter<number> = new EventEmitter<number>()

}
