import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'ma-users-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      (click)="setActive.emit(user)"
      class="list-group-item"
      [ngClass]="{'active': selected}"
    >
      {{user.name}}
      <div *ngIf="isOpened">
        {{user.id}}
        {{user.age}}
        {{user.city}}
      </div>

      <div class="pull-right">
        <i class="fa fa-arrow-circle-down mr-2" (click)="toggle($event)"></i>
        
        <i class="fa fa-trash mr-2"
           *ngIf="authService.showByRole('admin')"
           (click)="deleteHandler(user.id, $event)"></i>

        <!--<i class="fa fa-link" [routerLink]="'/users/'+ user.id"></i>-->

        <i class="fa fa-link"
           (click)="gotoHandler(user.id, $event)"></i>
      </div>
    </li>

  `,
})
export class UsersListItemComponent  {
  @Input() user: User;
  @Input() selected: boolean;
  @Output() setActive: EventEmitter<User> = new EventEmitter<User>()
  @Output() delete: EventEmitter<number> = new EventEmitter<number>()
  @Output() goto: EventEmitter<number> = new EventEmitter<number>()

  constructor(public authService: AuthService) {
  }
  isOpened = false;

  toggle(event: MouseEvent): void {
    event.stopPropagation();
    this.isOpened =  !this.isOpened
  }

  deleteHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();
    this.delete.emit(id);
  }


  gotoHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();
    // this.router.navigateByUrl('users/' + id)
    this.goto.emit(id)
  }
}
