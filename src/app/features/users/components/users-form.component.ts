import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../../model/user';

@Component({
  selector: 'ma-users-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <h1>{{title.text}}</h1>
    {{f.valid}}
    <form #f="ngForm" (submit)="saveHandler(f)">
      <input
        name="name"
        type="text"
        [ngModel]="active?.name"
        required
        minlength="3"
        #nameInput="ngModel"
        [style.border]="nameInput.invalid && f.dirty ? '1px solid red' : null"
      >
      <input name="age" type="number" [ngModel]="active?.age" required>
      <button type="submit" [disabled]="f.invalid">Save</button>
      <button type="button" (click)="resetHandler(f)">reset</button>
    </form>
  `,
})
export class UsersFormComponent implements OnChanges {
  @ViewChild('f', { static: true }) form: NgForm;
  @Input() active: User;
  @Input() title: any;
  @Output() save: EventEmitter<User> = new EventEmitter<User>();
  @Output() clean: EventEmitter<void> = new EventEmitter<void>();

  constructor(private cd: ChangeDetectorRef) {
    setTimeout(() => {
      cd.detectChanges();
    })
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.active) {
      if (!changes.active.currentValue.id) {
        this.form.reset();
      }
    }

   /* if (changes.title) {
      console.log('change title')
    }*/
  }
  saveHandler(form: NgForm): void {
    this.save.emit(form.value);
  }

  resetHandler(form: NgForm): void {
    this.clean.emit()
    form.reset();
  }
}
