import { Injectable } from '@angular/core';
import { User, UserAddedFormResponse } from '../../../model/user';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class UsersService {
  users: User[];
  active: User = {} as User;

  constructor(private http: HttpClient, private router: Router) {}

  getAll(): void {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => this.users = res);
  }

  deleteUser(id: number): void {
    this.http.delete(`http://localhost:3000/users/${id}`)
      .subscribe(() => {
        this.users = this.users.filter(item => item.id !== id)
        if (this.active.id === id) {
          this.active = {} as User;
        }
      });
  }

  setActive(user: User): void {
    this.active = user;
  }

  save(formData: User): Observable<UserAddedFormResponse> {
     return this.active.id ? this.edit(formData) : this.add(formData);
  }

  edit(formData: User): Observable<User | null> {
    this.http.patch<User>(`http://localhost:3000/users/${this.active.id}`, formData)
      .subscribe(res => {
        this.users = this.users.map(user => {
          return user.id === this.active.id ?  res : user;
        })
      });
    return of(null);
  }

  add(formData: User): Observable<UserAddedFormResponse> {
    const req$ = this.http.post<UserAddedFormResponse>('http://localhost:3000/users', formData)
    req$.subscribe(res => {
      this.users = [...this.users, res as User];
      this.active = {} as User;
    });
    return req$;
  }

  goToSheet(id: number): void {
    this.router.navigateByUrl('users/' + id)
  }

  resetHandler(): void{
    this.active = { } as User;
  }
}
