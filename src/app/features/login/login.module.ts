import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { SigninComponent } from './components/signin.component';
import { LostpassComponent } from './components/lostpass.component';
import { RegistrationComponent } from './components/registration.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [LoginComponent, SigninComponent, LostpassComponent, RegistrationComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SigninComponent },
          { path: 'registration', component: RegistrationComponent },
          { path: 'lostpass', component: LostpassComponent },
          { path: '', redirectTo: 'signin'},
        ]
      },
    ])
  ]
})
export class LoginModule { }
