import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ma-signin',
  template: `
    {{authService.auth?.token}}
    <form #f="ngForm" (submit)="authService.login(f.value.username, f.value.password)">
      <input type="text" ngModel name="username" required>
      <input type="text" ngModel name="password" required>
      <button type="submit" [disabled]="f.invalid">SIGNIN</button>
    </form>

    
  `,
  styles: [
  ]
})
export class SigninComponent {

  constructor(public authService: AuthService, private router: Router) {
    if (authService.isLogged()) {
      this.router.navigateByUrl('admin');
    }
  }

}
