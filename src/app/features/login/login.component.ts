import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-login',
  template: `
    Welcome to login
  
    <hr>
    
    <router-outlet></router-outlet>
    
    <hr>
    <button routerLink="signin">signin</button>
    <button routerLink="registration">registration</button>
    <button routerLink="lostpass">lostpass</button>
  `,
})
export class LoginComponent {

}
