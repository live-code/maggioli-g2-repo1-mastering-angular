import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ma-home',
  template: `
    <div class="alert alert-danger" *ngIf="error">errore</div>
    {{data | json}}
  `
})
export class HomeComponent implements OnInit {
  error: boolean;
data: any;
  constructor(private http: HttpClient) {
    http.get('http://localhost:3000/home')
      .subscribe(
        res => this.data = res,
        err => this.error = true
      )
  }

  ngOnInit(): void {
  }

}
