import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';

@Component({
  selector: 'ma-users-details',
  template: `
    {{data?.name}}
    {{data?.city}}
    {{data?.gender}}
  `,
  styles: [
  ]
})
export class UsersDetailsComponent implements OnInit {
  data: User;

  constructor(private activated: ActivatedRoute, private http: HttpClient) {
    const id = activated.snapshot.params.id;
    http.get<User>('http://localhost:3000/users/' +  id)
      .subscribe(res => this.data = res)
  }

  ngOnInit(): void {
  }

}
