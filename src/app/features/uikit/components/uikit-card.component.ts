import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-uikit-card',
  template: `

      <ma-card
        title="Bluetooth"
        icon="fa fa-bluetooth"
        [isOpen]="visible"
        (toggle)="visible = !visible"
        (iconClick)="openUrl('http://www.bluetooth.com')"
      >
        xxxxxxx
      </ma-card>

      <ma-card
        [config]="{ type: 'bar', color: 'orange'}"
        [title]="'Fabio'"
        icon="fa fa-link"
        [marginBottom]="2"
        [isOpen]="true"
        (iconClick)="doSomethingElse()"
      >
        <input type="text">
        <input type="text">
        <input type="text">
      </ma-card>

      <pre>{{visible}}</pre>
      <ma-card
        headerCls="bg-dark text-white"
        [isOpen]="visible"
        (toggle)="visible = !visible"
      >
        <button>Hello</button>
      </ma-card>

      {{visible ? 'pannello aperto' : 'panelllo chiuso'}}
      <hr>

  `,
  styles: [
  ]
})
export class UikitCardComponent  {
  visible = true;

  doSomethingElse(): void {
    console.log('something')
  }

  openUrl(url: string): void {
    console.log('open')
    window.open(url)
  }
}
