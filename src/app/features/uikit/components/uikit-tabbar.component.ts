import { Component, OnInit } from '@angular/core';
import { City, Country } from '../../../model/country';

@Component({
  selector: 'ma-uikit-tabbar',
  template: `
   
    {{activeCity |json}}
    <ma-tabbar
      [items]="countries"
      [active]="activeCountry"
      (tabClick)="tabCLickHandler($event)"
    ></ma-tabbar>

    <ma-tabbar
      labelField="name"
      [items]="activeCountry.cities" 
      [active]="activeCity"
      (tabClick)="activeCity = $event"
    ></ma-tabbar>
    
    <ma-card [title]="activeCity?.name" [isOpen]="true">
      <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + activeCity?.name + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
    </ma-card>
  `,
})
export class UikitTabbarComponent  {
  countries: Country[];
  activeCountry: Country;
  activeCity: City;

  constructor() {
    this.countries =  [
      {
        id: 1001,
        label: 'UK',
        description: 'bla bla 1',
        cities: [
          { id: 101, name: 'London'},
          { id: 102, name: 'Leads'},
        ]
      },
      {
        id: 1002,
        label: 'Italy',
        description: 'bla bla 2',
        cities: [
          { id: 105, name: 'Rome'},
          { id: 106, name: 'Milan'},
          { id: 1037, name: 'Bologna'},
        ]
      },
      {
        id: 1003,
        label: 'Spain',
        description: 'bla bla 3',
        cities: [
          { id: 1072, name: 'Madrid'},
        ]
      }
    ];
    this.tabCLickHandler( this.countries[0] );
  }

  tabCLickHandler(tab: Country): void {
    this.activeCountry = tab;
    this.activeCity = this.activeCountry.cities[0]
  }
}
