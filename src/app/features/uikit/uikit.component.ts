import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ma-uikit',
  template: `
    <h1>UIKIT</h1>
    <button routerLink="card">Card</button>
    <button routerLink="tabbar">Tabbar</button>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class UikitComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
