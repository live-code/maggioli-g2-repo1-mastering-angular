import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UikitCardComponent } from './components/uikit-card.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { UikitComponent } from './uikit.component';
import { UikitTabbarComponent } from './components/uikit-tabbar.component';

@NgModule({
  declarations: [
    UikitComponent,
    UikitCardComponent,
    UikitTabbarComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: UikitComponent,
        children: [
          { path: 'card', component: UikitCardComponent},
          { path: 'tabbar', component: UikitTabbarComponent},
        ]
      },
    ])
  ]
})
export class UikitModule { }
