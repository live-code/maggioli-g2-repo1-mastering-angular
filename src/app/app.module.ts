import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { UikitTabbarComponent } from './features/uikit/components/uikit-tabbar.component';
import { CoreModule } from './core/core.module';
import { AppRoutingModule, routes } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    // UikitTabbarComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    HttpClientModule,
    // SharedModule,
    AppRoutingModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
