import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'ma-navbar',
  template: `
    <button routerLink="login">login</button>
    <button routerLink="admin" 
            *ngIf="authService.showByRole('admin')">admin</button>
    <button routerLink="home">home</button>
    <button routerLink="uikit">uikit</button>
    <button routerLink="users" 
            *ngIf="authService.showByRole('moderator')">users</button>
    <button (click)="authService.logout()">logout</button>
    -
    {{authService.auth?.displayName}}
  `,
})
export class NavbarComponent {
  constructor(public authService: AuthService) {
  }

}

// locatstorage con obj
// directives custom
