export interface Auth {
  token: string;
  role: string;
  displayName: string;
}
