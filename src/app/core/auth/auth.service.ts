import { Auth } from './auth';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth: Auth | null = null;

  constructor(private http: HttpClient, private router: Router) {
      console.log(this.auth?.role)
  }

  login(user: string, pass: string): void {
    const params = new HttpParams()
      .set('user', user)
      .set('pass', pass);

    this.http.get<Auth>(`http://localhost:3000/login`, { params })
      .subscribe(res => {
        this.auth = res;
        this.router.navigateByUrl('admin');
        localStorage.setItem('token', res.token)
        localStorage.setItem('role', res.role)
      });
  }

  logout(): void {
    this.auth = null;
    localStorage.removeItem('token')
    localStorage.removeItem('role')
    this.router.navigateByUrl('login');

  }

  isLogged(): boolean {
    // return !!this.auth
    return !!localStorage.getItem('token');
  }

  showByRole(role: string): boolean {
    return this.isLogged() && (
      this.getRole() === role ||
      this.getRole() === 'admin'
    )
  }

  getToken(): string  {
    return localStorage.getItem('token');
  }
  getRole(): string  {
    return localStorage.getItem('role');
  }
}
