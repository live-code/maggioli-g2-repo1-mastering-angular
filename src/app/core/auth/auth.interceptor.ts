import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { catchError, delay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloned = req;
    if (this.authService.isLogged() && req.url.includes('users')) {
      cloned = req.clone({
        setHeaders: { Authorization: this.authService.getToken()}
      });
    }
    return next.handle(cloned)
      .pipe(
        delay(1000),
        catchError(err => {
          console.log('ERROREE!!!', req)
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 401:
                this.authService.logout();
                break;
              default:
              case 404:
                break;
            }
          }
          return throwError(err)
        })
      )
  }

}
