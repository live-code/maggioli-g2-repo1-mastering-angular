export interface User {
  id: number;
  name: string;
  age: number;
  bitcoins: number;
  gender: string;
  birthday: any;
  city: string;
}


// alias types
export type UserAddedForm =  Pick<User, 'name' | 'age'>;
export type UserAddedFormResponse =  Pick<User, 'id' | 'name' | 'age'>;

/*
// Union Type
export type RoleUser = Guest | Admin
// Intersection
export type SuperUser = Admin & SuperAdmin
1
function (user: RoleUser) {
  // .... type predicate --> Auth Guard
  if (user ===....) {
    user.
  } else {
    user.
  }
}
*/
